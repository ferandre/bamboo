package id.amigos.bamboo.webservice;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Base64;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import id.amigos.bamboo.util.Util;

/**
 * Created by Ferico Samuel on 27/10/2015.
 */
public class BambooWS {
    public interface Callback extends Response.Listener<JSONObject>, Response.ErrorListener {
    }

    private final RequestQueue requestQueue;
    private Context context;

    public BambooWS(Context context) {
        this.requestQueue = Volley.newRequestQueue(context);
        this.context = context;
    }

    public void login(String username, String password, Callback callback)
    {
        password = Util.md5(password);
        HashMap<String, String> param = new HashMap<>();
        param.put("apiKey",Util.API_KEY);
        param.put("username",username);
        param.put("password",password);
        requestQueue.add(new RestfulJsonRequest(Request.Method.POST,Util.LOGIN_URL,param,callback,callback));
    }

    public void register(String username, String firstName, String lastName, String password, String email, String phone, String address, String pickUp, boolean isCook, boolean isHunger,Bitmap photo, Callback callback)
    {
        password = Util.md5(password);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
        byte[] byteArray = outputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        HashMap<String, String> param = new HashMap<>();
        param.put("apiKey",Util.API_KEY);
        param.put("username",username);
        param.put("firstName",firstName);
        param.put("lastName",lastName);
        param.put("password",password);
        param.put("email",email);
        param.put("phone", phone);
        param.put("address",address);
        param.put("pickUpLocation",pickUp);
        param.put("isCook",isCook?"1":"0");
        param.put("isHunger",isHunger?"1":"0");
        param.put("photo",encoded);

        requestQueue.add(new RestfulJsonRequest(Request.Method.POST,Util.REGISTER_URL,param,callback,callback));
    }

    public void updateProfile(String username, String firstName, String lastName, String password, String email, String phone, String address, String pickUp, boolean isCook, boolean isHunger,Bitmap photo, Callback callback)
    {
        password = Util.md5(password);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 60, outputStream);
        byte[] byteArray = outputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        HashMap<String, String> param = new HashMap<>();
        param.put("apiKey",Util.API_KEY);
        param.put("username",username);
        param.put("firstName",firstName);
        param.put("lastName",lastName);
        param.put("password",password);
        param.put("email",email);
        param.put("phone", phone);
        param.put("address",address);
        param.put("pickUpLocation",pickUp);
        param.put("isCook",isCook?"1":"0");
        param.put("isHunger",isHunger?"1":"0");
        param.put("photo",encoded);

        requestQueue.add(new RestfulJsonRequest(Request.Method.POST,Util.REGISTER_URL,param,callback,callback));
    }
}
