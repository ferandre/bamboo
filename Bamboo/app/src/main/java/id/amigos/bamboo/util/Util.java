package id.amigos.bamboo.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import id.amigos.bamboo.model.User;

/**
 * Created by Ferico Samuel on 31/10/2015.
 */
public class Util {
    private static final String BASE_URL = "http://www.ferware.com/WSBamboo/";
    private static final String SHARED_PREFERENCE_KEY = "id.amigos.bamboo";

    public static final String API_KEY = "99e914ffa379151bc68ed4faa2fa6dc1";
    public static final String LOGIN_URL = BASE_URL+"login.php";
    public static final String REGISTER_URL = BASE_URL+"register.php";

    public static void saveUserData(User user, Context context)
    {
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREFERENCE_KEY,Context.MODE_PRIVATE);
        pref.edit().putString("userID",user.getUserID());
        pref.edit().putString("verificationStatus",user.getVerificationStatus());
        pref.edit().putString("firstName",user.getFirstName());
        pref.edit().putString("lastName",user.getLastName());
        pref.edit().putString("email",user.getEmail());
        pref.edit().putString("phone",user.getPhone());
        pref.edit().putString("address",user.getAddress());
        pref.edit().putString("pickUpLocation",user.getPickUpLocation());
        pref.edit().putBoolean("isCook",user.isCook());
        pref.edit().putBoolean("isHunger",user.isHunger());
        pref.edit().putBoolean("isVerified",user.isVerified());
        pref.edit().putInt("point",user.getPoint());
        pref.edit().putFloat("rating",(float)user.getRating());
    }

    public static User getUserData(Context context)
    {
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREFERENCE_KEY,Context.MODE_PRIVATE);
        User user = new User();
        user.setUserID(pref.getString("userID", ""));
        user.setVerificationStatus(pref.getString("verificationStatus", ""));
        user.setFirstName(pref.getString("firstName", ""));
        user.setLastName(pref.getString("lastName", ""));
        user.setEmail(pref.getString("email", ""));
        user.setPhone(pref.getString("phone", ""));
        user.setAddress(pref.getString("address", ""));
        user.setPickUpLocation(pref.getString("pickUpLocation", ""));
        user.setIsCook(pref.getBoolean("isCook", false));
        user.setIsHunger(pref.getBoolean("isHunger", false));
        user.setIsVerified(pref.getBoolean("isVerified", false));
        user.setPoint(pref.getInt("point", 0));
        user.setRating((double) pref.getFloat("rating", 0));
        return user;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
