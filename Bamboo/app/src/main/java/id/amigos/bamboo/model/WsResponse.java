package id.amigos.bamboo.model;

import org.json.JSONObject;

/**
 * Created by Ferico Samuel on 27/10/2015.
 */
public class WsResponse {
    JSONObject jsonObject;

    public void parse(JSONObject jsonObject)
    {
        this.jsonObject = jsonObject;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }
}
