package id.amigos.bamboo.webservice;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ferico Samuel on 31/10/2015.
 */
public class RestfulJsonRequest extends StringRequest {
    Map<String,String> params;
    String uri;
    Response.Listener<JSONObject> listener;

    public RestfulJsonRequest(int method, String uri,
                              Map<String, String> params, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, uri, null, errorListener);
        this.params = params;
        this.listener = listener;
        this.uri = uri;

        if (method == Method.GET) {
            for (Map.Entry<String, String> entry : params.entrySet()) {

                String encodedValue = entry.getValue();

                try {
                    encodedValue = URLEncoder.encode(entry.getValue(), "UTF-8")
                            .replaceAll("\\+", "%20");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                this.uri = this.uri.replace("{" + entry.getKey() + "}", encodedValue);
            }
        }


        this.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }
    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        if (getMethod() == Method.GET)
            return super.getParams();
        else
            return params;
    }

    @Override
    public String getUrl() {
        return uri;
    }
    @Override
    protected void deliverResponse(String response) {

        Log.v("request", uri);

        try {
            Log.v("response", new JSONObject(response).toString(1));
        } catch (JSONException e) {
            Log.v("response", response);
        }

        try {
            listener.onResponse(new JSONObject(response));
        } catch (JSONException e) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("data", new JSONArray(response));
                listener.onResponse(obj);
            } catch (JSONException e1) {
                e1.printStackTrace();
                deliverError(new VolleyError(e));
            }
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String,String> params = new HashMap<String, String>();
        params.put("Content-Type","application/x-www-form-urlencoded");
        return params;
    }


}