package id.amigos.bamboo;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import id.amigos.bamboo.model.User;
import id.amigos.bamboo.service.RegistrationIntentService;
import id.amigos.bamboo.util.Util;
import id.amigos.bamboo.webservice.BambooWS;

public class MainActivity extends AppCompatActivity implements BambooWS.Callback {

    private RelativeLayout splashLayout,loginLayout,firstLayout,registerLayout,forgotLayout;
    private ImageView imgLogo,imgLogo2;
    private EditText txtFirstEmail,txtRegisterDOB;
    private int animationDuration = 1000;
    private int splashDuration = 3000;
    private final static int FIRST_STEP = 111;
    private final static int LOGIN_STEP = 222;
    private final static int REGISTER_STEP = 333;
    private final static int FORGOT_STEP = 444;
    private int step = FIRST_STEP;
    private float dpHeight;
    private float density;
    private int year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        splashLayout = (RelativeLayout)findViewById(R.id.splashLayout);
        loginLayout = (RelativeLayout) findViewById(R.id.loginLayout);
        firstLayout = (RelativeLayout) findViewById(R.id.firstLayout);
        registerLayout = (RelativeLayout) findViewById(R.id.registerLayout);
        forgotLayout = (RelativeLayout) findViewById(R.id.forgotLayout);
        imgLogo = (ImageView) findViewById(R.id.imgLogo1);
        imgLogo2 = (ImageView) findViewById(R.id.imgLogo2);
        txtFirstEmail = (EditText) findViewById(R.id.txtFirstEmail);
        txtRegisterDOB = (EditText) findViewById(R.id.txtRegisterDOB);

        txtRegisterDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        year = selectedyear;
                        month = selectedmonth;
                        day = selectedday;
                        txtRegisterDOB.setText(day + " - " + month + " - " + year);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });

        Intent intent = new Intent(this,RegistrationIntentService.class);
        startService(intent);
        splashLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                removeSplash();
            }
        }, splashDuration);

        TextView txtForgot = (TextView) findViewById(R.id.txtForgot);
        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleForgot(true);
            }
        });

        LinearLayout createLayout = (LinearLayout) findViewById(R.id.layoutCreateAccount);
        createLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toggleRegister(true);
            }
        });
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            if(response.getBoolean("success"))
            {
                User user = new User(response);
                Util.saveUserData(user,getApplicationContext());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        if(step==0)
            super.onBackPressed();
        else
        {
            switch(step)
            {
                case LOGIN_STEP:
                    toggleLogin(false);
                    break;
                case FIRST_STEP:
                    super.onBackPressed();
                    break;
                case REGISTER_STEP:
                    toggleRegister(false);
                    break;
                case FORGOT_STEP:
                    toggleForgot(false);
                    break;
            }
        }
    }

    public void removeSplash()
    {
        Animation alphaAnim = new AlphaAnimation(1,0);
        alphaAnim.setDuration(animationDuration);
        alphaAnim.reset();
        splashLayout.clearAnimation();
        splashLayout.startAnimation(alphaAnim);

        Animation revAlphaAnim = new AlphaAnimation(0,1);
        revAlphaAnim.setDuration(animationDuration);
        revAlphaAnim.reset();
        firstLayout.clearAnimation();
        firstLayout.setVisibility(View.VISIBLE);
        firstLayout.startAnimation(revAlphaAnim);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);

        density  = getResources().getDisplayMetrics().density;
        dpHeight = outMetrics.heightPixels;

        Animation imgAnim = new TranslateAnimation(0,0,0,-(dpHeight/2)+90*density);
        imgAnim.setDuration(animationDuration);
        imgAnim.reset();
        imgLogo.clearAnimation();
        imgLogo.startAnimation(imgAnim);

        imgLogo.postDelayed(new Runnable() {
            @Override
            public void run() {
                imgLogo.setVisibility(View.GONE);
                splashLayout.setVisibility(View.GONE);
                imgLogo2.setVisibility(View.VISIBLE);
            }
        }, animationDuration);

        txtFirstEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.v("FS", "txt first email focus change");
                if (hasFocus) {
                    toggleLogin(hasFocus);
                }
            }
        });
    }

    private void toggleLogin(boolean show)
    {
        if(show)
        {
            step = LOGIN_STEP;

            LinearLayout firstEmailLayout = (LinearLayout) findViewById(R.id.firstEmailLayout);
            Animation translateAnimation = new TranslateAnimation(0,0,0,-(dpHeight/2)+200*density);
            translateAnimation.setDuration(animationDuration-400);
            translateAnimation.reset();
            firstEmailLayout.clearAnimation();
            firstEmailLayout.startAnimation(translateAnimation);

            loginLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation alphaAnim = new AlphaAnimation(1,0);
                    alphaAnim.setDuration(animationDuration-200);
                    alphaAnim.reset();
                    firstLayout.clearAnimation();
                    firstLayout.startAnimation(alphaAnim);

                    loginLayout.setVisibility(View.VISIBLE);
                    Animation revAnim = new AlphaAnimation(0,1);
                    revAnim.setDuration(animationDuration-200);
                    revAnim.reset();
                    loginLayout.clearAnimation();
                    loginLayout.startAnimation(revAnim);
                }
            },animationDuration-800);

            loginLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    firstLayout.setVisibility(View.GONE);
                    loginLayout.setVisibility(View.VISIBLE);
                }
            },animationDuration);
        }
        else
        {
            step = FIRST_STEP;

            txtFirstEmail.setOnFocusChangeListener(null);

            Animation alphaAnim = new AlphaAnimation(1,0);
            alphaAnim.setDuration(animationDuration);
            alphaAnim.reset();
            loginLayout.clearAnimation();
            loginLayout.startAnimation(alphaAnim);

            firstLayout.setVisibility(View.VISIBLE);
            Animation revAnim = new AlphaAnimation(0,1);
            revAnim.setDuration(animationDuration);
            revAnim.reset();
            firstLayout.clearAnimation();
            firstLayout.startAnimation(revAnim);

            firstLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    loginLayout.setVisibility(View.GONE);
                }
            }, animationDuration);

            firstLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    txtFirstEmail.clearFocus();
                    txtFirstEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            Log.v("FS", "txt first email focus change");
                            if (hasFocus) {
                                toggleLogin(hasFocus);
                            }
                        }
                    });
                }
            },animationDuration+50);
        }
    }

    private void toggleRegister(boolean show)
    {
        if(show)
        {
            registerLayout.setVisibility(View.VISIBLE);
            step = REGISTER_STEP;

            Animation alphaAnim = new AlphaAnimation(1,0);
            alphaAnim.setDuration(animationDuration);
            alphaAnim.reset();
            firstLayout.clearAnimation();
            firstLayout.startAnimation(alphaAnim);

            Animation revAlphaAnim = new AlphaAnimation(0,1);
            revAlphaAnim.setDuration(animationDuration);
            revAlphaAnim.reset();
            registerLayout.clearAnimation();
            registerLayout.startAnimation(revAlphaAnim);

            registerLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    firstLayout.setVisibility(View.GONE);
                }
            }, animationDuration);
        }
        else
        {
            firstLayout.setVisibility(View.VISIBLE);
            step = FIRST_STEP;

            Animation alphaAnim = new AlphaAnimation(1,0);
            alphaAnim.setDuration(animationDuration);
            alphaAnim.reset();
            registerLayout.clearAnimation();
            registerLayout.startAnimation(alphaAnim);

            Animation revAlphaAnim = new AlphaAnimation(0,1);
            revAlphaAnim.setDuration(animationDuration);
            revAlphaAnim.reset();
            firstLayout.clearAnimation();
            firstLayout.startAnimation(revAlphaAnim);

            firstLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    registerLayout.setVisibility(View.GONE);
                }
            }, animationDuration);
        }
    }

    private void toggleForgot(boolean show)
    {
        if(show)
        {
            forgotLayout.setVisibility(View.VISIBLE);
            step = FORGOT_STEP;

            Animation alphaAnim = new AlphaAnimation(1, 0);
            alphaAnim.setDuration(animationDuration);
            alphaAnim.reset();
            loginLayout.clearAnimation();
            loginLayout.startAnimation(alphaAnim);

            Animation revAlphaAnim = new AlphaAnimation(0, 1);
            revAlphaAnim.setDuration(animationDuration);
            revAlphaAnim.reset();
            forgotLayout.clearAnimation();
            forgotLayout.startAnimation(revAlphaAnim);

            forgotLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    loginLayout.setVisibility(View.GONE);
                }
            }, animationDuration);
        }
        else
        {
            loginLayout.setVisibility(View.VISIBLE);
            step = LOGIN_STEP;

            Animation alphaAnim = new AlphaAnimation(1, 0);
            alphaAnim.setDuration(animationDuration);
            alphaAnim.reset();
            forgotLayout.clearAnimation();
            forgotLayout.startAnimation(alphaAnim);

            Animation revAlphaAnim = new AlphaAnimation(0, 1);
            revAlphaAnim.setDuration(animationDuration);
            revAlphaAnim.reset();
            loginLayout.clearAnimation();
            loginLayout.startAnimation(revAlphaAnim);

            loginLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    forgotLayout.setVisibility(View.GONE);
                }
            }, animationDuration);
        }
    }
}
