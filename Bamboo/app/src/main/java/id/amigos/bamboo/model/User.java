package id.amigos.bamboo.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ferico Samuel on 31/10/2015.
 */
public class User {
    private String userID;
    private String verificationStatus;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String address;
    private String pickUpLocation;
    private boolean isCook;
    private boolean isHunger;
    private boolean isVerified;
    private int point;
    private double rating;

    public User()
    {

    }

    public User(JSONObject json) throws JSONException
    {
        json = json.getJSONObject("user");
        userID = json.getString("userID");
        verificationStatus = json.getString("verificationStatus");
        firstName = json.getString("firstName");
        lastName = json.getString("lastName");
        email = json.getString("email");
        phone = json.getString("phone");
        address = json.getString("address");
        pickUpLocation = json.getString("pickUpLocation");
        isCook = json.getBoolean("isCook");
        isHunger = json.getBoolean("isHunger");

        isVerified = json.getBoolean("isVerified");
        point = json.getInt("point");
        rating = json.getDouble("rating");
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setVerificationStatus(String verificationStatus) {
        this.verificationStatus = verificationStatus;
    }

    public boolean isHunger() {
        return isHunger;
    }

    public void setIsHunger(boolean isHunger) {
        this.isHunger = isHunger;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserID() {
        return userID;
    }

    public String getVerificationStatus() {
        return verificationStatus;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public boolean isCook() {
        return isCook;
    }

    public void setIsCook(boolean isCook) {
        this.isCook = isCook;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setIsVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
